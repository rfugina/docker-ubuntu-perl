
FROM ubuntu:jammy
MAINTAINER Rob Fugina <rfugina@wustl.edu>
ENV DEBIAN_FRONTEND noninteractive

# Update package lists and install updates
RUN apt-get update && apt-get -y install apt-transport-https \
  && sed -e 's/^deb-src/# deb-src/' -i /etc/apt/sources.list \
  && sed -e 's/^# deb /deb /' -i /etc/apt/sources.list \
  && apt-get update && apt-get -y install apt-utils build-essential git man vim iproute2 \
  && apt-get -y dist-upgrade && apt-get -y autoremove && apt clean

# Perl stuff I've decided that I need
COPY perl_packages.txt perl_modules.txt /tmp/
RUN apt-get -y install $(grep -v ^# /tmp/perl_packages.txt) && apt clean
RUN cpan $(grep -v ^# /tmp/perl_modules.txt) && rm -rvf /root/.local/share/.cpan/{Metadata,build,sources}

WORKDIR /root

COPY test_script.sh /tmp/

