#!/bin/bash

set -e -x

RC=0

for module in $(grep -v ^# /tmp/perl_modules.txt)
do
    /usr/bin/env perl -M"${module}" -e 'exit 0' || RC=1
done

exit $RC

